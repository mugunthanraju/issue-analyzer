import gitlab
import csv

def commit_detail(required_file):

    return 5

def deep_files(folder):
    pass


if __name__ == "__main__":

    # user_name = "garywong"
    user_name = input("Enter GitLab User Name : ")
    Url = "https://" + user_name + "@gitlab.com"

    gl = gitlab.Gitlab(Url, per_page=50)
    projects = gl.projects.list(owned=False)

    for project_index in range(len(projects)):
        print(f"{project_index + 1} ) {projects[project_index].name} - {projects[project_index].id}")

    project_index = int(input("Enter an index number of project you wish to see the report : ")) - 1

    repo_detail = [
        ["Repository_Files", "Repository_Name", "Repository_URL", "Repo_No.of_Commits"],
    ]

    for fil in projects[project_index].repository_tree():
        repo_detail.append([fil["name"]])

    repo_detail[1].append(projects[project_index].name)
    repo_detail[1].append(projects[project_index].web_url)
    repo_detail[1].append(len(projects[project_index].commits.list()))    

    with open('report.csv', 'w', encoding='UTF8', newline='') as f:
        writer = csv.writer(f)
        writer.writerows(repo_detail)